{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h1 align='center'> Using Terminal</h1>\n",
    "<h3 align='center'> Terminal Commands, Git commands and Editor Shortcuts (emacs) </h3>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Why do we need Terminal?\n",
    "\n",
    "1. It is standardized through POSIX and the Single Unix Specification, so a script you write for one computer will likely work on all `POSIX` compliant machines (assuming you restrict yourself to standard commands, of which there are lots). \n",
    "\n",
    "1. UNIX was built from the terminal up, most everything is configurable from the command line.\n",
    "\n",
    "1. It is so flexible. Commands can be piped together `ls | grep filename`, they can be captured `gcc program.c > ./standard_out 2> ./standard_error`, and can be substituded `ls /home/$(whoami)`\n",
    "\n",
    "1. UNIX utilities are designed to do one thing, and do it well. Just look into `awk,grep,sed, wget` or a host of others. By themselves they achieve a single task, but given #3 and #2 they can be built into powerful expressions.\n",
    "\n",
    "1. The ability to automate tasks. cron and bash scripts allow long, complicated, and/or repetitive tasks to either be simplified or automated completely."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Basic Terminal Commands\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### pwd (Print Current Working Directory)\n",
    "The Terminal session maintains a so-called current working directory. All relative pathnames/filenames are relative to the current working directory. To display the current directory, issue command \"pwd\" (print working directory):\n",
    "```\n",
    "// Print Current Working Directory\n",
    "$ pwd\n",
    "......\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### cd (Change Working Directory)\n",
    "To change the current working directory, issue command \"cd <new-pathname>\". You can specify new-pathname in two ways: absolute or relative. As explained earlier, an absolute path begins with a \"/\" (root directory) or \"~\" (home directory); whereas a relative path is relative to the current working directory and does NOT begin with \"/\" or \"~\". For example,\n",
    "```\n",
    "$ cd /                // Change directory (absolute) to the root\n",
    "$ cd /usr/local       // Change directory (absolute) to \"/usr/local\"\n",
    "$ cd share            // Change directory (relative) to share of the current directory\n",
    "$ cd doc/homebrew     // Change directory (relative) to doc/homebrew of the current directory\n",
    "```\n",
    "\n",
    "You can cd in multiple stages (e.g., one cd for each sub-directory), or cd in a single stage with the full pathname.\n",
    "```\n",
    "$ cd /         // \"/\"\n",
    "$ cd usr       // \"/usr\"\n",
    "$ cd local     // \"/usr/local\"\n",
    "$ cd share     // \"/usr/local/share\"\n",
    "$ cd doc       // \"/usr/local/share/bin\"\n",
    " \n",
    "// Same As\n",
    "$ cd /usr/local/share/doc\n",
    "```\n",
    "\n",
    "You can use \"/\" to denote the root; \"~\" to refer to your home directory; \"..\" (double-dot) to refer to the parent directory; \".\" (single-dot) to refer to the current directory; and \"-\" (dash) to refer to the previous directory. For example,\n",
    "```\n",
    "$ cd ~            // Change directory to the home directory of the current user\n",
    "$ cd              // same as above, default for \"cd\" is home directory\n",
    "$ cd ~/Documents  // Change directory to the sub-directory \"Documents\" of the home directory of the current user\n",
    "$ cd ..           // Change directory to the parent directory of the current working directory\n",
    "$ cd -            // Change directory to the previous working directory (OLDPWD)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ls (List Directory's Contents)\n",
    "\n",
    "You can use command ls to list the contents of the current working directory, e.g.,\n",
    "```\n",
    "// List contents of current working directory in short format\n",
    "$ ls\n",
    "Desktop    Downloads         Music     Public     Videos\n",
    "Documents  examples.desktop  Pictures  Templates\n",
    " \n",
    "// List in long format\n",
    "$ ls -l\n",
    "total 24\n",
    "drwx------   6 mohitsharma44  staff   204 Nov  5 07:58 Applications\n",
    "drwx------+  4 mohitsharma44  staff   136 Nov  8 00:16 Desktop\n",
    "drwx------+ 18 mohitsharma44  staff   612 Dec 24 10:49 Documents\n",
    "drwx------+ 30 mohitsharma44  staff  1020 Jan  5 22:06 Downloads\n",
    "......\n",
    "\n",
    "// List all files including hidden files in long format\n",
    "$ ls -la\n",
    "total 216\n",
    "drwx------    7 mohitsharma44  staff    238 Dec  2 15:54 .ssh\n",
    "drwxr-xr-x    6 mohitsharma44  staff    204 Jul  1  2015 .subversion\n",
    "-rw-r--r--    1 mohitsharma44  staff     37 Nov 24 12:22 .tmux.conf\n",
    "-rw-r--r--    1 mohitsharma44  staff     21 Nov 24 12:22 .tmux.conf~\n",
    "-rw-------    1 mohitsharma44  staff   1466 Aug 29 17:54 .viminfo\n",
    "drwxr-xr-x    6 mohitsharma44  staff    204 Sep 19 13:02 .wireshark\n",
    "drwx------    6 mohitsharma44  staff    204 Nov  5 07:58 Applications\n",
    "drwx------+   4 mohitsharma44  staff    136 Nov  8 00:16 Desktop\n",
    "drwx------+  18 mohitsharma44  staff    612 Dec 24 10:49 Documents\n",
    "drwx------+  30 mohitsharma44  staff   1020 Jan  5 22:06 Downloads\n",
    "...\n",
    "\n",
    "\n",
    "```\n",
    "\n",
    "Wildcard *\n",
    "You can list selected files using wildcard *, which matches 0 or more (any) characters. For example,\n",
    "```\n",
    "$ ls *.java     // List files ending with \".java\" in short format (default)\n",
    "$ ls -l *.java  // List files ending with \".java\" in long format\n",
    "$ ls -ld my*    // List files and directories beginning with \"my\" in long format\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### | (pipe)\n",
    "You can connect two commands together so that the output from one program becomes the input of the next program. Two or more commands connected in this way form a pipe.\n",
    "\n",
    "To make a pipe, put a vertical bar (|) on the command line between two commands. For example,\n",
    "```\n",
    "// This command will perform ls and 'pipe' the output to another command 'wc' which basically counts the number of lines. (This is same as the number of files/ folders in the home directory)\n",
    "\n",
    "$ ls ~/ | wc -l\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### grep \n",
    "The grep program searches a file or files for lines that have a certain pattern. For example,\n",
    "```\n",
    "$ ls -l | grep Jan\n",
    "    // This will find keyword Jan in the output of ls -l command\n",
    "drwx------+ 30 mohitsharma44  staff  1020 Jan  5 22:06 Downloads\n",
    "drwxr-xr-x  11 mohitsharma44  staff   374 Jan  4 15:37 devel\n",
    ".....\n",
    "```\n",
    "\n",
    "For case independent matching, use -i flag"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### cat (catenate and list files)\n",
    "The program cat is a standard Unix utility that reads files sequentially, writing them to standard output. The name is derived from its function to catenate and list files. For example,\n",
    "```\n",
    "// To read or read the contents of files, \n",
    "$ cat /etc/passwd\n",
    "```\n",
    "\n",
    "The above command will display the contents of a file named /etc/passwd. By default cat will send output to the monitor screen. But, you can redirect from the screen to another command or file using redirection operator as follows,\n",
    "```\n",
    "$ cat /etc/passwd > /tmp/test.txt\n",
    "```\n",
    "\n",
    "In the above example, the output from cat command is written to /tmp/text.txt file instead of being displayed on the monitor screen. You can view /tmp/text.txt using cat command itself:\n",
    "```\n",
    "$ cat /tmp/test.txt\n",
    "```\n",
    "\n",
    "Concatenate files\n",
    "\n",
    "Concatenation means putting multiple file contents together. The original file or files are not modified or deleted. \n",
    "```\n",
    "// In this example, cat will concatenate copies of the contents of the three files /etc/hosts, /etc/resolv.conf, and /etc/fstab:\n",
    "\n",
    "$ cat /etc/hosts /etc/resolv.conf /etc/fstab\n",
    "```\n",
    "\n",
    "You can redirect the output as follows using shell standard output redirection:\n",
    "```\n",
    "$ cat /etc/hosts /etc/resolv.conf /etc/fstab > /tmp/outputs.txt\n",
    "$ cat /tmp/outputs.txt\n",
    "```\n",
    "\n",
    "You can also use a pipe to filter data. In this example send output of cat to the less command using a shell pipe as the file is too large for all of the text to fit on the screen at a time:\n",
    "```\n",
    "$ cat /etc/passwd | less\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### less (Viewing File Contents)\n",
    "You can use commands less to display the contents of a text file on console. For example,\n",
    "```\n",
    "$ less /proc/cpuinfo\n",
    "  // Display one page of the file\n",
    "  // Use Up|Down|PgUp|PgDown key to scroll, and type \"q\" to quit\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### man (Help manual)\n",
    "\n",
    "You can use man to find help for any command. For example,\n",
    "```\n",
    "$ man ls\n",
    "    // Display help for ls command\n",
    "    // Use Up|Down|PgUp|PgDown key to scroll, and type \"q\" to quit\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### touch (modify/ create timestamp for file)\n",
    "\n",
    "Change file timestamps. Sets the modification and access times of files to the current time of day. If the file doesn't exist, it is created with default permissions. For example,\n",
    "\n",
    "```\n",
    "$ touch test.txt\n",
    "    // Will create a file test.txt if it doesn't exist. If it does, it will change the modify time to current time.\n",
    "$ touch -c test.txt\n",
    "    // Do not create the file if it does not exist.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### sudo\n",
    "\n",
    "Allows a permitted user to execute a command as the superuser or another user, as specified in the sudoers file. For example,\n",
    "\n",
    "```\n",
    "sudo touch /test.txt\n",
    "    // This will prompt for sudo password and if input correctly, create a file in root dir (/)\n",
    "sudo rm /test.txt\n",
    "    // This will remove the file test.txt without asking for password\n",
    "    \n",
    "```\n",
    "Once the terminal session is authenticated, it will not ask for password for next 15 minutes. i.e. If you try running the above commands in two separate sessions, you'll have to enter password for every session."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### clear (Clear the console)\n",
    "\n",
    "Simply put, this clears the current window. (Within Terminal in OS X, you can still scroll up to see what was there. This command simply clears the current view).\n",
    "```\n",
    "$ clear\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----------\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Basic Git Commands"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### git config (Tell Git who you are)\n",
    "\n",
    "Configure the author name and email address to be used with your commits.\n",
    "Note that Git strips some characters (for example trailing periods) from user.name.\n",
    "\n",
    "Commands:\n",
    "```\n",
    "$ git config --global user.name \"John Doe\"\n",
    "$ git config --global user.email john@johndoe.com\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### git init (Create New/ Empty git repository)\n",
    "The git init command creates a new Git repository. It can be used to convert an existing, unversioned project to a Git repository or initialize a new empty repository. Most of the other Git commands are not available outside of an initialized repository, so this is usually the first command you’ll run in a new project.\n",
    "\n",
    "Executing git init creates a .git subdirectory in the project root, which contains all of the necessary metadata for the repo.\n",
    "\n",
    "Command:\n",
    "```\n",
    "$ git init\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### git clone (Checkout a repository to local machine)\n",
    "\n",
    "This command will checkout a git respository to local machine.\n",
    "\n",
    "Command:\n",
    "```\n",
    "$ git clone username@host:/path/to/repository\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### git add (Add files for staging)\n",
    "Add one or more files to staging (index):\n",
    "\n",
    "Command:\n",
    "```\n",
    "$ git add <filename>\n",
    "\n",
    "$ git add *\n",
    "\n",
    "$ git add --all\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### git commit\n",
    "Commit changes to head (but not yet to the remote repository)\n",
    "\n",
    "Command:\n",
    "``` \n",
    "$ git commit -m \"Commit message\" \n",
    "```\n",
    "\n",
    "Commit any files you've added with git add, and also commit any files you've changed since then\t\n",
    "\n",
    "Command:\n",
    "``` \n",
    "$ git commit -a \n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### git push\n",
    "Send changes to the master branch of your remote repository\n",
    "\n",
    "Command:\n",
    "```\n",
    "$ git push origin master\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### git status\n",
    "List the files you've changed and those you still need to add or commit\n",
    "\n",
    "Command:\n",
    "```\n",
    "$ git status\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### git remote\n",
    "The git remote command lets you create, view, and delete connections to other repositories.\n",
    "\n",
    "Create a new connection to a remote repository. After adding a remote, you’ll be able to use <name> as a convenient shortcut for <url> in other Git commands.\n",
    "\n",
    "Command:\n",
    "```\n",
    "$ git remote add <name> <server>\n",
    "\n",
    "```\n",
    "\n",
    "Rename a remote connection from <old-name> to <new-name>.\n",
    "\n",
    "Command:\n",
    "```\n",
    "$ git remote rename <old-name> <new-name>\n",
    "\n",
    "```\n",
    "\n",
    "List all currently configured remote repositories.\n",
    "\n",
    "Command:\n",
    "```\n",
    "$ git remote -v\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### branches\n",
    "A branch represents an independent line of development. Branches serve as an abstraction for the edit/stage/commit process. You can think of them as a way to request a brand new working directory, staging area, and project history. New commits are recorded in the history for the current branch, which results in a fork in the history of the project.\n",
    "\n",
    "The git branch command lets you create, list, rename, and delete branches. It doesn’t let you switch between branches or put a forked history back together again.\n",
    "\n",
    "Create a new branch and switch to it:\n",
    "```\n",
    "$ git checkout -b <branchname>\n",
    "```\n",
    "\n",
    "Switch from one branch to another:\t\n",
    "```\n",
    "$ git checkout <branchname>\n",
    "```\n",
    "\n",
    "List all the branches in your repo, and also tell you what branch you're currently in:\t\n",
    "```\n",
    "$ git branch\n",
    "```\n",
    "\n",
    "Delete the feature branch:\n",
    "```\n",
    "$ git branch -d <branchname>\n",
    "```\n",
    "\n",
    "Push the branch to your remote repository, so others can use it:\n",
    "```\n",
    "$ git push origin <branchname>\n",
    "```\n",
    "\n",
    "Push all branches to your remote repository:\n",
    "```\n",
    "$ git push --all origin\n",
    "```\n",
    "\n",
    "Delete a branch on your remote repository:\n",
    "```\n",
    "$ git push origin :<branchname>\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### git pull\n",
    "Update from remote repository\n",
    "\n",
    "Command:\n",
    "```\n",
    "$ git pull\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### git merge\n",
    "To merge different branch to current branch\n",
    "\n",
    "Command:\n",
    "```\n",
    "$ git merge <branchname>\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### git diff\n",
    "View difference/ conflicts between current state and most recent commit\n",
    "\n",
    "Command:\n",
    "```\n",
    "$ git diff\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Tags\n",
    "You can use tagging to mark a significant changeset, such as a release.\n",
    "\n",
    "Command:\n",
    "```\n",
    "$ git tag 1.0.0 <commitID>\n",
    "```\n",
    "> get the commitID using `$ git log`\n",
    "\n",
    "Push all tags to remote repository:\t\n",
    "```\n",
    "$ git push --tags origin\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Undo local changes\n",
    "To drop all your local changes and commits, fetch the latest history from the server and point your local master branch at it, do this:\n",
    "```\n",
    "$ git fetch origin\n",
    "\n",
    "$ git reset --hard origin/master\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### search\n",
    "To search for a particular string in the git.\n",
    "\n",
    "Command:\n",
    "```\n",
    "git grep \"foo()\"\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---------"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Emacs\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Opening, Creating, Modifying a file\n",
    "`emacs -nw <filename>`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Closing a file\n",
    "`Ctrl+x Ctrl+c`\n",
    "### Saving and closing a file\n",
    "`Ctrl+x Ctrl+s`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Copying selected text\n",
    "```\n",
    "# Highlight/ select using Shift+<arrow buttons>\n",
    "Esc+w \n",
    "```\n",
    "### Pasting selected text\n",
    "```\n",
    "Ctrl+y\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
